Instructions utilisation framework : 

- ajouter annotation ClassesAnnotation pour les classes à utiliser
	@ClassesAnnotation
	public class Test{
	}

- les url doit se terminer par .do
- mettre les names des éléments input même nom aux attributs de l'objet à manipuler

- Type de données de retour de fonction de type ModeleView
	- set url du ModeleView vers la page(.jsp)
	- set hashmap data ModeleView en l'objet de retour 

		public ModeleView getAll(){
			ModeleView example = new ModeleView();
			example.setUrl("liste.jsp");
 			HashMap<String, Object> data = new HashMap<String, Object>();
        		data.put("listeName", arrayOfData);
			example.setData(data);

			return example
		}

- ajouter annotations UrlAnnotation pour les méthodes à invoqués : 	
	- url
	- class 
	- nom de la méthode
	@UrlAnnotation(url="url", binded="nom du classe", methodName="nom de la méthode")	
	public ModeleView getAll(){
	}



Le .jar du framework est dans TestFrameworkETU1588/FrameworkETU1588.jar