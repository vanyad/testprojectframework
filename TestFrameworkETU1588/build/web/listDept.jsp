<%@page import="models.*"%>
<%@page import="java.util.ArrayList"%>
<%
   ArrayList<Departement> departements = (ArrayList<Departement>)request.getAttribute("listeDept");
%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
       <table>
            <thead class="thead-primary">
                <tr>
                    <th>ID</th>
                    <th>Nom</th>
                </tr>
            </thead>
            <tbody id="listeCandidat">
                <% for(Departement departement: departements ){ %>
                    <tr>
                        <td><%= departement.getId()%></td>
                        <td><%= departement.getNom()%></td>
                    </tr>
                <% } %>
            </tbody>
        </table>
    </body>
</html>
