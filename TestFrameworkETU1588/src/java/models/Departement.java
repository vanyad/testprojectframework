/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import utilities.ObjetBDD;
import annotations.ClassesAnnotation;
import annotations.UrlAnnotation;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.HashMap;
import utilities.ModelView;

@ClassesAnnotation
public class Departement extends ObjetBDD{
    int id;
    String nom;
   
    public Departement(int id, String nom) {
        this.id = id;
        this.nom = nom;
    }
    public Departement(String nom) {
        this.nom = nom;
    }
    
    public Departement() {
    }

    public int getId() {
        return id;
    }

    public String getNom() {
        return nom;
    }

    public void setId(int id) {
        this.id = id;
    }
    
    public void setNom(String nom) {
        this.nom = nom;
    }
    
    @UrlAnnotation(url = "getAllDept", binded = Departement.class, methodName = "getAll" )
    public ModelView getAll() throws Exception {
        ArrayList<Departement> array = new ArrayList<Departement>();
     
        array.add(new Departement(1, "Info" ));
        array.add(new Departement(2, "RH" ));
        array.add(new Departement(3, "Compta" ));
        
        ModelView result = new ModelView();
        result.setUrl("listDept.jsp");
        HashMap<String, Object> data = new HashMap<String, Object>();
        data.put("listeDept", array);
        result.setData(data);
        
        return result;
    }
    
    @UrlAnnotation(url = "createDept", binded = Departement.class, methodName = "createDept" )
    public ModelView createDept() throws Exception {
        this.create(null);
        return getAll();
    }
   
}
