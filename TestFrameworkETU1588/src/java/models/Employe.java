/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import annotations.ClassesAnnotation;
import annotations.UrlAnnotation;
import java.sql.Date;

/**
 *
 * @author RKT
 */
@ClassesAnnotation
public class Employe {
    int id;
    String nom;
    Date dateNaissance;
    Departement idDept;

    public Employe(String nom, Date dateNaissance, int id, Departement dept) {
        this.nom = nom;
        this.dateNaissance = dateNaissance;
        this.id = id;
        this.idDept = dept;
    }

    public Employe() {
    }
    

    public String getNom() {
        return nom;
    }

    public Date getDateNaissance() {
        return dateNaissance;
    }

    public int getId() {
        return id;
    }

    public Departement getDept() {
        return idDept;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public void setDateNaissance(Date dateNaissance) {
        this.dateNaissance = dateNaissance;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setDept(Departement dept) {
        this.idDept = dept;
    }
    
    @UrlAnnotation(url = "getAllEmp", binded = Employe.class, methodName = "getAll" )
    public void getAll(){
        
    }
}
